﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task29
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is Task 29?
            //Task 29 is to 'Count the number of words in a string'
                            //The quick brown fox jumped over the fence

            //Store the variables
            string sentence = "The quick brown fox jumped over the fence";
            string[] separators = { " ", ",", ".", "!", "?", ":", ";" };
            var input = "0";

            //Program start. Explain what is going to happen to the user
            Console.WriteLine("Hi there user. You're about to experience some awesome. The next line is an example of a string.");
            Console.WriteLine(sentence);
            Console.WriteLine();
            Console.WriteLine("What I'm not going to do is tell you how many words were in that sentence.");
            
            //Split the string variable 'sentence' into an array using spaces to consider what an item is
            string[] words = sentence.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine();
            Console.WriteLine("There are {0} words in that sentence.", words.Length);
            Console.WriteLine("Don't believe I did it? Then type in your own sentence.");
            Console.WriteLine("Remember words have spaces");
            input = Console.ReadLine();
            
            //Split the variable 'input' into an array using spaces to consider what an item is
            string[] words2 = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine();
            Console.WriteLine("I think you'll find that there are {0} words in that sentence.", words2.Length);

        }
    }
}
