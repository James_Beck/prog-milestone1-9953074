﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task07
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is Task 07
            //Task 07 is to 'Forloop - show times table from 1 x to 12 x for a given number - each answer on a new line'

            //Storage of variables
            double num;
            int failsafe = 100;

            //Explain the program to the user
            Console.WriteLine("Hi there. This program lets you find the first 12 multiples of a number of your choice.");
            Console.WriteLine("Please make sure to input the number in its numerical form.");
            Console.WriteLine("If you don't input a number then the number in question will be 100.");
            Console.WriteLine();

            //Get the user to put in a value.
            Console.WriteLine("Please input a number of your choice.");
            try
            {
                num = double.Parse(Console.ReadLine());
            }
            catch
            {
                num = failsafe;
                Console.WriteLine("That wasn't a valid input, as such the default value of '100' is going to be used.");
            }
            Console.WriteLine();

            //Use a Forloop to give the answer to the first twelve multiples of that number
            for (int i = 1; i < 13; i++)
            {
                if (i % 10 == 1 && i != 11)
                {
                    Console.WriteLine("The {0}st multiple is equal to {1}", i, num * i);
                }
                else if (i % 10 == 2 && i != 12)
                {
                    Console.WriteLine("The {0}nd multiple is equal to {1}", i, num * i);
                }
                else if (i % 10 == 3)
                {
                    Console.WriteLine("The {0}rd multiple is equal to {1}", i, num * i);
                }
                else
                {
                    Console.WriteLine("The {0}th multiple is equal to {1}", i, num * i);
                }
            }
        }
    }
}
