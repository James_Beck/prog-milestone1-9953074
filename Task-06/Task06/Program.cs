﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task06
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is Task 06
            //Task 06 is to 'Forloop - calculate the sum of 5 numbers to n (n given to you by user)'

            //Storage of the Variables
            string inputs;
            double store;
            double answer = 0;

            //Start program, Get user to input the numbers
            Console.WriteLine("Hi there user, I'm going to need to get 5 numbers from your");
            Console.WriteLine("Words are counted as 0's");
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Enter, in it's numerical form, a number.");
                inputs = Console.ReadLine();
                double.TryParse(inputs, out store);
                answer = answer + store;
                Console.Clear();
            }
            //Show the sum value of the number inputs that the user has input
            Console.WriteLine("That sums to {0}.", answer);
        }
    }
}
