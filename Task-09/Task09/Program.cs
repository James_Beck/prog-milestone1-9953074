﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task09
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is Task 09
            //Task 09 is to 'Write a program that shows which years are leap years for the next 20 years'

            //Storage of variables
            int currentyear = 2016;
            string choiceyear;
            int isyear;

            //Program start. Program explanation for the user
            Console.WriteLine("Hi there. This program does two things.");
            Console.WriteLine("1) It tells the user which of the next twenty years are leap years.");
            Console.WriteLine("2) It also lets the user pick any year of choice and tell you the leap years over the next 20 years.");
            Console.WriteLine();

            //Forloop and if statement for the current year
            Console.WriteLine("The current year is {0}.", currentyear);
            Console.WriteLine("Of the next twenty years");
            for (int i = 1; i < 21; i++)
            {
                if (currentyear % 4 == 0 && i != 1 && currentyear % 100 != 0 || currentyear % 400 == 0 && currentyear != 0)
                {
                    Console.WriteLine(currentyear);
                    currentyear++;
                }
                else
                {
                    currentyear++;
                }
            }
            Console.WriteLine("These are the leap years.");
            Console.WriteLine();
            Console.WriteLine("If you would like to continue to the next part of the program, press any key.");
            Console.ReadKey();

            //User input version
            Console.Clear();
            Console.WriteLine("Please input a year. (Any written text is going to make it equal to 0)");
            choiceyear = Console.ReadLine();
            int.TryParse(choiceyear, out isyear);
            Console.WriteLine();
            Console.WriteLine("If the current year is {0}.", isyear);
            Console.WriteLine("Of the next twenty years");
            for (int i = 1; i < 23; i++)
            {
                if (isyear % 4 == 0 && i != 1 && isyear % 100 != 0 || isyear % 400 == 0 && isyear != 0)
                {
                    Console.WriteLine(isyear);
                    isyear++;
                }
                else
                {
                    isyear++;
                }
            }
        }
    }
}
