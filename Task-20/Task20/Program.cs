﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task20
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is Task 20
            //Task 20 is to 'The array for you to use has the following values (34, 45, 21, 44, 67, 88, 86)
                            //a) Put in all the odd numbers in a new list
                            //b) Print the List

            //Store the variables
            List<int> oddnum = new List<int>();
            var numarray = new int[7] { 34, 45, 21, 44, 67, 88, 86 };
            //Program start. Communicate to the user
            Console.WriteLine("Hi there. In this program you are going to see that I can extract a list of even numbers from an array and print the extracted list");
            Console.WriteLine("The array from which we're taking the information is [34, 45, 21, 44, 67, 88, 86]");
            for (int i = 0; i < numarray.Length; i++)
            {
                if (numarray[i] % 2 == 1)
                {
                    oddnum.Add(numarray[i]);
                }
                else { }
            }
            Console.WriteLine();
            Console.WriteLine($"The odd numbers within the array are...");
            for (int i = 0; i < oddnum.Count; i++)
            {
                if (i + 1 == oddnum.Count)
                {
                    Console.WriteLine(oddnum[i]);
                }
                else
                {
                    Console.Write("{0}, ", oddnum[i]);
                }
            }
        }
    }
}
