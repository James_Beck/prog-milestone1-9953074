﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task33
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is Task 33?
            //Task 33 is to 'Write a program that will tell you how many tutorial groups need to be made based on a number given by the user.'
                            //Each tutorial group has a maximum of 28 students.

            //Store variables
            string tutorial;
            double result;

            //Program start. Explanation to the user
            Console.WriteLine("Hi there. I'm going to require that you input a number.");
            Console.WriteLine("This number is a representation of a chosen number of students.");
            Console.WriteLine("This number will the be divided among tutorial groups; of a maximum size of 28.");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            //User input for variable 'tutorial'
            do
            {
                Console.Clear();
                Console.WriteLine("So, how many students are there in this scenario?");
                tutorial = Console.ReadLine();
                double.TryParse(tutorial, out result);
            } while (result <= 0);
            Console.WriteLine();
            Console.WriteLine("Great, so that was {0} students", tutorial);
            //Gives options. Where there are a remainder of students, the tutorial groups increase by 1
            if (result % 28 == 0)
            {
                Console.WriteLine("Great, that means there are {0} tutorial classes.", result / 28);
            }
            else
            {
                Console.WriteLine("Great, that means there are {0} tutorial classes.", Math.Ceiling(result / 28));
            }
        }
    }
}
