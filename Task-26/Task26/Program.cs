﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task26
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is Task 26?
            //Task 26 is to 'Create an array with the following values and print it in descending order to the screen).
                            //[Red, Blue, Yellow, Green, Pink]

            //Store of variables
            string[] colours = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink"};

            //Program start. Explanation to the user
            Console.WriteLine("Hi there. I have an array of colours");
            Console.Write("The colours are: ");
            //Show the original status of the array to the user
            for (int i = 0; i < colours.Length; i++)
            {
                if (i + 1 == colours.Length)
                {
                    Console.WriteLine("and {0}", colours[i]);
                }
                else
                {
                    Console.Write("{0}, ", colours[i]);
                }
            }
            Console.WriteLine();
            Console.WriteLine("Now I'm going to put them in descending order for you, just cause you're special.");
            Console.WriteLine();
            //Sort the order of the array, then reverse this order
            Array.Sort(colours);
            Array.Reverse(colours);
            Console.Write("The reverse order is: ");
            for (int i = 0; i < colours.Length; i++)
            {
                if (i + 1 == colours.Length)
                {
                    Console.WriteLine("and {0}", colours[i]);
                }
                else
                {
                    Console.Write("{0}, ", colours[i]);
                }
            }
        }
    }
}
