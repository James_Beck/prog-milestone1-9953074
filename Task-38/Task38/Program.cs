﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task38
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is task 38
            //Task 38 is to 'Forloop - show division table from 1 x to 12 x for a given number - each answer on a new line'

            //Store variables
            double input;

            //Program start. Explanation to the user.
            Console.WriteLine("Hi there. This program gets you, the user, to input a number.");
            Console.WriteLine("If you don't input a number, then an example number of 1200 will be used.");
            Console.WriteLine("After that the program divides the number by a set of numbers; those being {1:12}.");

            //Get user input for variable 'input'
            try
            {
                input = double.Parse(Console.ReadLine());
                Console.WriteLine("Great now let's see what happens when we divide {0} by the set.", input);
            }
            catch
            {
                input = 1200;
                Console.WriteLine("Ok, now let's see what happens when we divide {0} by the set.", input);
            }
            Console.WriteLine();

            //show values for variable 'input' when divided by numbers {1:12}
            for (int i = 1; i < 13; i++)
            {
                Console.WriteLine("When divided by {0}, {1} = {2}", i, input, input / i);
            }
        }
    }
}
