﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task40
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is task 40
            //Task 40 is to 'Assuming that the 1st of the month is on a Monday (like this month) write a program that tells you how many Wednesdays are in a month.'

            //Store variables
            string[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            int[] days = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            int month;
            string input;
            int wednesdays = 0;

            //Program start. Explanation to the user
            Console.WriteLine("Hi there user, this program tells you how many Mondays are in a month given that the month begins on a monday.");
            Console.WriteLine("It is a leap year this year, so February is a month with 29 days.");
            Console.Write("Press any key to continue");
            Console.ReadKey();

            //After program pause gets the user to input a value for the month
            do
            {
                Console.Clear();
                Console.WriteLine("So, as a numeric, enter the month you're interested in");
                input = Console.ReadLine();
                int.TryParse(input, out month);
            } while (month < 1 || month > 12);

            Console.WriteLine();
            for (int i = 0; i < days[month - 1] - 2; i++)
            {
                if (i % 7 == 0)
                {
                    wednesdays++;
                }
                else { }
            }
            Console.WriteLine("{0} is a month that has {1} days in it and has {2} wednesdays.", months[month - 1], days[month - 1], wednesdays);
        }
    }
}


            
