﻿using System;
//Necessary namespace to permit a dictionary
using System.Collections.Generic;
//End of required permission
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task23
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is task 23
            //Task 23 is to 'Create a dictionary filled with days of the week and weekends'
                            // a) 'Display to the user which days are week days and which are weekends.'

            //Storage of variables
            var DOTW = new Dictionary<string, string>();

            //Values for the dictionary
            DOTW.Add("Monday", "weekday");
            DOTW.Add("Tuesday", "weekday");
            DOTW.Add("Wednesday", "weekday");
            DOTW.Add("Thursday", "weekday");
            DOTW.Add("Friday", "weekday");
            DOTW.Add("Saturday", "weekend");
            DOTW.Add("Sunday", "weekend");

            //Program start. Explain the program
            Console.WriteLine("Hi there. This program is a dictionary. It will display to the user which days are week days and which are weekends");
            Console.WriteLine();
            foreach (var x in DOTW)
            {
                Console.WriteLine("{0} is a {1}", x.Key, x.Value);
            }
            Console.WriteLine();      
        }
    }
}
