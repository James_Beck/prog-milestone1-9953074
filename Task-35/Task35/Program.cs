﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task35
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is task 35?
            //Task 35 is to 'Write a program that asks 5 different maths questions based on 3 numbers by user input. Display the answer on the screen.'

            //Store variables
            string input;
            double output;
            int a = 1;
            List<double> questions = new List<double>();
            List<double> answers = new List<double>();
            string correct = "correct";


            //Program start. Explanation to the user
            Console.WriteLine("Hi there. This program is here to pose 5 questions based on 3 numbers of your choosing.");
            Console.WriteLine("Make sure you input the numbers in their numerical form. If they are not, they'll be counted as a zero.");
            for (int i = 0; i < 3; i++)
            {
                input = Console.ReadLine();
                double.TryParse(input, out output);
                questions.Add(output);
            }
            answers.Add(questions[0] + questions[1] + questions[2]);
            answers.Add(questions[0] - questions[1] + questions[2]);
            answers.Add(questions[0] + questions[1] - questions[2]);
            answers.Add(questions[0] - questions[1] - questions[2]);
            answers.Add(questions[0] / questions[1] * questions[2]);

            //Displays all five questions to the user       
            Console.Clear();
            Console.WriteLine("Great, what is {0} + {1} + {2}?", questions[0], questions[1], questions[2]);
            if (Console.ReadLine() == Convert.ToString(answers[0]))
            {
                Console.WriteLine(correct);
            }
            else { }
            Console.WriteLine("Great, what is {0} - {1} + {2}?", questions[0], questions[1], questions[2]);
            if (Console.ReadLine() == Convert.ToString(answers[1]))
            {
                Console.WriteLine(correct);
            }
            else { }
            Console.WriteLine("Great, what is {0} + {1} - {2}?", questions[0], questions[1], questions[2]);
            if (Console.ReadLine() == Convert.ToString(answers[2]))
            {
                Console.WriteLine(correct);
            }
            else { }
            Console.WriteLine("Great, what is {0} - {1} - {2}?", questions[0], questions[1], questions[2]);
            if (Console.ReadLine() == Convert.ToString(answers[3]))
            {
                Console.WriteLine(correct);
            }
            else { }
            Console.WriteLine("Great, what is {0} / {1} * {2}?", questions[0], questions[1], questions[2]);
            if (Console.ReadLine() == Convert.ToString(answers[4]))
            {
                Console.WriteLine(correct);
            }
            else { }
        }
    }
}
