﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task34
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is task 34?
            //Task 34 is to 'Write a program that tells you how many working days there are when the user types in the number of weeks.'

            //Store variables
            string working;
            double output;
            double store;

            //Program start. Explain to the user what is happening
            Console.WriteLine("Hi there. This program is going to tell you how many working days are in the upcoming weeks.");
            Console.WriteLine("Working days are the 5 weekdays of each week.");
            Console.WriteLine("You'll be asked to input a value for an amount of weeks. Only use the numerical version");
            Console.WriteLine();
            Console.WriteLine("Alright, so how many weeks would you like to analyze?");
            
            //User input for the variable 'working'
            do
            {
                working = Console.ReadLine();
                double.TryParse(working, out output);
            } while (output <= 0);
            store = output * 5;
            Console.WriteLine();
            Console.WriteLine("Great, I think you'll find that's {0} workdays.", Math.Ceiling(store));

        }
    }
}
