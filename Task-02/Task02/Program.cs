﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is Task 2?
            //Task 2 is to 'get user input for the month and day they are born on and display it in a sentence'

            //Storage place of the variables
            int month;
            int day;
            var input = "0";
            string month29 = "February";
            Dictionary<int, string> months = new Dictionary<int, string>();
            months.Add(1, "January");
            months.Add(3, "March");
            months.Add(4, "April");
            months.Add(5, "May");
            months.Add(6, "June");
            months.Add(7, "July");
            months.Add(8, "August");
            months.Add(9, "September");
            months.Add(10, "October");
            months.Add(11, "November");
            months.Add(12, "December");

            //Program start, ask the user for the information about the day of their birth
            //The do while loop forces a numerical input for the day that is not equal to 0
            do
            {
                Console.WriteLine("Hi there. Please input, in its numerical form, the day in which you were born on.");
                input = Console.ReadLine();
                int.TryParse(input, out day);
                //If statement to limit the numbers to a range of whole numbers from {1:31}
                if (day > 31 || day < 0)
                {
                    day = 0;
                }
                else
                { }
                Console.Clear();
            } while (day == 0);
            //Ask the user for the information about the month they were born in
            do
            {
                Console.Clear();
                Console.WriteLine("So in it's numerical form, what month of the year were you born?");
                input = Console.ReadLine();
                int.TryParse(input, out month);
            } while (month <= 0 || month > 12);

            //This part combines the information from variables 'month' and 'day' and communicates them to the user in a sentence.
            //The If Statements combine the information from the month and day to see two things
                    //1) Is it possible. For example, 31st of February is impossible
                    //2) Which array do I get my value for month from?
            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
            {
                Console.WriteLine("Thank you.");
                Console.WriteLine("The program is now concluded. You were born on day {0} in the month of {1}.", day, months[month]);
            }
            else if (month == 4 || month == 6 || month == 9 || month == 11 && day <= 30)
            {
                Console.WriteLine("Thank you.");
                Console.WriteLine("The program is now concluded. You were born on day {0} in the month of {1}.", day, months[month]);
            }
            else if (month == 2 && day <= 29)
            {
                Console.WriteLine("Thank you.");
                Console.WriteLine("The program is now concluded. You were born on day {0} in the month of {1}.", day, month29);
            }
            else
            {
                Console.WriteLine("Hey Now! That's not possible!");
                Console.WriteLine();
            }
        }
    }
}
