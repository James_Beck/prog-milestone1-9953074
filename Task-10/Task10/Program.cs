﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is task 10
            //Task 10 is to 'Write a program that tells you how many leap years are there in the next 20 years (with calculation)'

            //Storage of variables
            int currentyear = 2016;
            int four = 4;
            int hundred = 100;
            int fourhundred = 400;
            int zero = 0;
            bool answer = false;

            //Program start. Program explanation for the user
            Console.WriteLine("Hi there. This program does two things.");
            Console.WriteLine("1) It tells the user which of the next twenty years are leap years with calculation.");
            Console.WriteLine("2) It also lets the user pick any year of choice and tell them the leap years over the next 20 years with calculation.");
            Console.WriteLine();

            //Forloop and if statement for the current year
            Console.WriteLine("The current year is {0}.", currentyear);
            Console.WriteLine("Of the next twenty years");
            Console.WriteLine();
            for (int i = 1; i < 21; i++)
            {
                if (currentyear % four == 0 && i != 1 && currentyear % hundred != 0 || currentyear % fourhundred == 0 && currentyear != zero)
                {
                    Console.WriteLine("{0} is a leap year because:", currentyear);
                    Console.WriteLine("1a) It has no remainder when divided by 4. ({0} % {1} = {2})", currentyear, four, currentyear % four);
                    Console.WriteLine("1b) Or it has no remainder when divided by 400. ({0} % {1} = {2})", currentyear, fourhundred, currentyear % fourhundred);
                    Console.WriteLine("2) And when divided by 100 is leaves a remainder. ({0} % {1} = {2})", currentyear, hundred, currentyear % hundred);
                    if (currentyear != 0)
                    {
                        answer = true;
                    }
                    else { }
                    Console.WriteLine("3) And the current year {0} is not equal to {1}. ({0} != {1} is {2})", currentyear, zero, answer);
                    currentyear++;
                    Console.WriteLine();
                }
                else
                {
                    currentyear++;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Every other year in the next 20 years does not meet this requirement as is not a leap year.");
        }
    }
}
