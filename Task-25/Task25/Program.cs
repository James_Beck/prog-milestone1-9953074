﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task25
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is task 25?
            //Task 25 is to 'Write a program that takes user input for a number and cannot crash'
                            //Check what happens when you get a word
                            //Check what happens when you get a number

            //Store of variables
            var input = "";

            //Program start. Explanation to the user
            Console.WriteLine("Hi there. This program won't crash no matter if you enter a number or word.");
            Console.WriteLine("Please input a number of your choice in numerical form");
            
            //Receives the user input
            input = Console.ReadLine();
            Console.Clear();

            //Attempts to convert the users input into an integer, when this fails it results with value == 0
            try
            {
                double.Parse(input);
                Console.WriteLine("Great, you typed {0}, a number!.", input);
            }
            catch
            {
                Console.WriteLine("So yeah, you fail! You get an F. Make than F-. '{0}' wasn't a number.", input);
            }
        }
    }
}
