﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task12
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is Task 12
            // Task 12 is to 'Check if a number is a even or odd number  (yes or no answer to the user)'

            //Storage of variables
            string usernum;
            int output;

            //Program start. Begin with an explanation to the user
            Console.WriteLine("Hi there. This program tells you whether the number you've input is odd or even.");
            Console.WriteLine("Please be sure to input a whole number in numerical form, if it isn't it will equal zero.");
            Console.WriteLine("Please input a number now.");
            Console.WriteLine();
            usernum = Console.ReadLine();
            int.TryParse(usernum, out output);
            Console.WriteLine("Great now let's see if {0} is odd or even", output);
            Console.WriteLine();
            if (output % 2 == 1)
            {
                Console.WriteLine("********************");
                Console.WriteLine("{0} is an odd number", output);
                Console.WriteLine("********************");
            }
            else
            {
                Console.WriteLine("********************");
                Console.WriteLine("{0} is an even number", output);
                Console.WriteLine("********************");
            }
        }
    }
}
