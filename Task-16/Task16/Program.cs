﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task16
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is task 16
            //Task 16 is to 'Count the letters in a word (Display the string and the number of characters it is)'

            //Storage of variables
            string user;
            string failsafe = "example";
            string[] separators = { " ", ",", ".", "!", "?", ":", ";", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" }; 

            //Program start. The program is going to receive information from the user to count the letters in a word
            Console.WriteLine("Hi there user. This program allows you to count the number of letters in a given word.");
            Console.WriteLine("Please input a word. If you do not, the word measured will be 'example'");
            Console.WriteLine("If you input multiple words, the first word is the one that will be examined.");
            Console.WriteLine();
            user = Console.ReadLine();
            string[] input = user.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine();

            //Program states the word and outputs how many letters it has
            try
            {
                Console.WriteLine("Cool, well you'll find that word {0} has {1} letters.", input[0], input[0].Length);
            }
            catch
            {
                Console.WriteLine("Well, the word in question now is going to be {0}. A word which has {1} letters.", failsafe, failsafe.Length);
            }
        }
    }
}
