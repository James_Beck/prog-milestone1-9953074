﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task37
{
    class Program
    {
        static void Main(string[] args)
        {
            //What is Task 37?
            //Task 37 is to 'Write a program that calculates how many hours you should study per week for a 15 credit paper.'
                            //Use the following data to create your app:
                            //a) There are 10 hours of study for every credit of the paper
                            //b) Each week gives 5 contact hours(hours in lecture and tutorials)
                            //c) The Semester is 12 weeks long.

            //Store variables
            int credits = 15;
            int study = 10;
            int contact = 5;
            int semester = 12;

            //Program start. Explanation to the user
            Console.WriteLine("Hi there. Let's say that you pick up a 15 credit paper, how much study would that mean for you?");
            Console.WriteLine("This program seeks to find out. Here is the important information:");
            Console.WriteLine();
            
            //Display of each individual variable input into the overall 'calculating hours' equation
            Console.WriteLine("Total credits for a paper: {0}", credits);
            Console.WriteLine("Total hours of study per credit: {0}", study);
            Console.WriteLine("Total lecture and tutorial hours per week: {0}", contact);
            Console.WriteLine("Total weeks in the semester: {0}", semester);
            Console.WriteLine();
            
            //Display of the value when variables are combined
            Console.WriteLine($"This means that you have to study for a total of {credits * study + contact * semester} hours");
        }
    }
}
